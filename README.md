# Post widget #

## Installation ##

* Clone the project: git clone -b develop --recursive `REPO URL`
* Execute follow command `composer install`
* Start dockerbox with follow command: `docker-compose up -d --build nginx mysql`
* Import database dump (If no database dump in data folder exists, create dump from live server and put it to data folder and push it to the repo!)
* Add follow host in your host file
* HAPPY PROGRAMMING!

## Host ##


* WIN: `C:\Windows\System32\drivers\etc`
* MacOS/Linux: `/etc/hosts`
````
127.0.0.1         post-widget.local
````

## Credentials ##

### Backend ###
* Username: admin
* Password: see lastPass

### Database ###
* Host: post_widget_mysql_1
* Database: post_widget
* Username: post_widget_u
* Password: post_widget_p

## URLs ##

Website
````
https://post-widget.local/
````

Backend
````
https://post-widget.local/####INSERT BACKEND PAGE HERE####
````

Mailhog
````
https://post-widget.local:8025/
````