#!/usr/bin/env bash

sshConnectionAlias='post_widget'

ssh $sshConnectionAlias 'bash -s' <<'ENDSSH'
  cd html/post_widget/preview
  git pull
  php_cli artisan october:up
  composer update
ENDSSH