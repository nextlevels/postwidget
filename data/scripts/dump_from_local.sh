#Variables

# LOCAL SETTINGS
localDockerDatabaseName='default'
sshConnectionAlias='post_widget'

# REMOTE MITTWALD MYSQL CONNETIONS
host='XXXXX.mydbserver.com'
user='pXXXXX'
password='DB_PASSWORD_HERE'

# IMPORTANT!!!!!
db_number=2  #DATABASE NUMBER FOR DUMP IMPORT
# IMPORTANT!!!!!

now=$(date +"%s")
filename='post_widget_local_'$now'.sql'

if [ ! -d "data/sql" ]; then
  mkdir data/sql
fi

docker-compose exec mysql /usr/bin/mysqldump --login-path=local $localDockerDatabaseName > 'data/sql/'$filename
cd data/sql

ssh $sshConnectionAlias 'bash -s' <<'ENDSSH'
  cd html
  if [ ! -d "sqldumps" ]; then
    mkdir sqldumps
  fi
ENDSSH

scp $filename $sshConnectionAlias:/html/sqldumps

if [ "$1" == "1" ]; then
    db='usr_'$user
    db_full=$db'_'$db_number
ssh $sshConnectionAlias 'bash -s' << EOF
    cd html/sqldumps
    mysql -h $host -u $user -p$password $db_full < $filename
EOF

fi