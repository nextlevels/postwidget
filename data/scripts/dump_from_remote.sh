#Variables

# LOCAL SETTINGS
localDockerDatabaseName='post_widget'
sshConnectionAlias='post_widget'

# REMOTE MITTWALD MYSQL CONNETIONS
host='XXXXX.mydbserver.com'
user='pXXXXX'
password='DB_PASSWORD_HERE'

# IMPORTANT!!!!!
db_number=4  #DATABASE NUMBER FOR DUMP IMPORT
# IMPORTANT!!!!!

filename='database_dump_post_widget_'$db_number'.sql'

if [ ! -d "data/sql" ]; then
  mkdir data/sql
fi

ssh $sshConnectionAlias 'bash -s' << EOF
  cd html
  if [ ! -d "sqldumps" ]; then
    mkdir sqldumps
  fi
  cd sqldumps
  mysqldump -h $host -u $user -p$password 'usr_'$user'_'$db_number  > $filename
EOF

scp $sshConnectionAlias:html/sqldumps/$filename data/sql