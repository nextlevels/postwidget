<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetProductsSmethods extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_products_smethods', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_products_smethods', function ($table) {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
