<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetDiscounts extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->integer('min_items')->nullable(false)->unsigned()->change();
            $table->decimal('discount', 10, 2)->nullable(false)->unsigned(false)->default(0.00)->change();
            $table->dropColumn('max_items');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->integer('min_items')->nullable()->unsigned(false)->change();
            $table->double('discount', 10, 0)->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('max_items')->nullable()->unsigned()->default(1000000000);
        });
    }
}
