<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials7 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->decimal('base_price', 10, 2)->default(0);
            $table->decimal('stack_price', 10, 2)->default(0);
            $table->decimal('express_stack_price', 10, 2)->default(0);
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->dropColumn('base_price');
            $table->dropColumn('stack_price');
            $table->dropColumn('express_stack_price');
        });
    }
}
