<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials6 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->integer('weight_all_gram_per_unit')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->dropColumn('weight_all_gram_per_unit');
        });
    }
}
