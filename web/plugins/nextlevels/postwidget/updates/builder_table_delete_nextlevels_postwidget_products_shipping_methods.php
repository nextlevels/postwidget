<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsPostwidgetProductsShippingMethods extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_shipping_methods');
    }

    public function down()
    {
        Schema::create('nextlevels_postwidget_products_shipping_methods', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('s_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
