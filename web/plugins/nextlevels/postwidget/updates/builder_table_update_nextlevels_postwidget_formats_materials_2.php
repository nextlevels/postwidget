<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormatsMaterials2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->decimal('gram_per_unit', 10, 4)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->decimal('gram_per_unit', 10, 0)->change();
        });
    }
}
