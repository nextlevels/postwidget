<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetProductsFormats extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_products_formats', function ($table) {
            $table->double('price', 10, 0);
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_products_formats', function ($table) {
            $table->dropColumn('price');
        });
    }
}
