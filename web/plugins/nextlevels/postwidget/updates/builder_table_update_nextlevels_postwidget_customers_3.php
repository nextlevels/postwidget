<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetCustomers3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_customers', function ($table) {
            $table->string('street', 191)->nullable()->change();
            $table->string('house_number', 191)->nullable()->change();
            $table->string('zip', 191)->nullable()->change();
            $table->string('country', 191)->nullable()->change();
            $table->string('city', 191)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_customers', function ($table) {
            $table->string('street', 191)->nullable(false)->change();
            $table->string('house_number', 191)->nullable(false)->change();
            $table->string('zip', 191)->nullable(false)->change();
            $table->string('country', 191)->nullable(false)->change();
            $table->string('city', 191)->nullable(false)->change();
        });
    }
}
