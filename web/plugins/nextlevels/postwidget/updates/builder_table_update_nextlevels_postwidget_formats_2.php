<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormats2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->decimal('height', 10, 0)->nullable(false)->unsigned(false)->default(0)->change();
            $table->decimal('width', 10, 0)->nullable(false)->unsigned(false)->default(0)->change();
            $table->integer('pages')->nullable(false)->default(1)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->double('height', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->double('width', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->integer('pages')->nullable()->default(null)->change();
        });
    }
}
