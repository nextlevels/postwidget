<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsPostwidgetProductsMaterials2 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_materials');
    }

    public function down()
    {
        Schema::create('nextlevels_postwidget_products_materials', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('m_id')->unsigned();
            $table->decimal('price', 10, 2)->default(0.00);
            $table->primary(['p_id', 'm_id']);
        });
    }
}
