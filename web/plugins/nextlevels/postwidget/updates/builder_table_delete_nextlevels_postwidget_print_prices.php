<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsPostwidgetPrintPrices extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_postwidget_print_prices');
    }

    public function down()
    {
        Schema::create('nextlevels_postwidget_print_prices', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->integer('min_items')->unsigned();
            $table->decimal('value', 10, 2);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('material_id')->unsigned();
        });
    }
}
