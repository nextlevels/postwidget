<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetCustomers2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_customers', function ($table) {
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('city', 191);
            $table->string('salutation', 191)->nullable()->change();
            $table->string('house_number', 191)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('zip', 191)->nullable(false)->unsigned(false)->default(null)->change();
            $table->dropColumn('forename');
            $table->dropColumn('name');
            $table->dropColumn('place');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_customers', function ($table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('city');
            $table->string('salutation', 191)->nullable(false)->change();
            $table->integer('house_number')->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('zip')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('forename', 191);
            $table->string('name', 191);
            $table->string('place', 191);
        });
    }
}
