<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials5 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->renameColumn('product_id', 'format_id');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->renameColumn('format_id', 'product_id');
        });
    }
}
