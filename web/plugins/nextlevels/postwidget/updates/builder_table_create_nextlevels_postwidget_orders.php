<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetOrders extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_orders', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->date('date');
            $table->integer('product_id')->unsigned();
            $table->integer('material_id')->unsigned();
            $table->integer('format_id')->unsigned();
            $table->integer('shipping_method_id')->unsigned();
            $table->text('zips')->nullable();
            $table->text('delivery_areas')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_orders');
    }
}
