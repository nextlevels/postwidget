<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetFormatsMaterials extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_formats_materials', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('f_id')->unsigned();
            $table->integer('m_id')->unsigned();
            $table->decimal('gram_per_unit', 10, 0);
            $table->primary(['f_id', 'm_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_formats_materials');
    }
}
