<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetOrders2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_orders', function ($table) {
            $table->dateTime('ordered_at');
            $table->dropColumn('date');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_orders', function ($table) {
            $table->dropColumn('ordered_at');
            $table->date('date');
        });
    }
}
