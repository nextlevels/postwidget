<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetFormats extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_formats', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->double('height', 10, 0)->nullable();
            $table->double('width', 10, 0)->nullable();
            $table->string('alignment')->nullable();
            $table->integer('pages')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_formats');
    }
}
