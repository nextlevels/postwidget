<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_postwidget_material', 'nextlevels_postwidget_materials');
    }

    public function down()
    {
        Schema::rename('nextlevels_postwidget_materials', 'nextlevels_postwidget_material');
    }
}
