<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsPostwidgetFormatsMaterials extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_postwidget_formats_materials');
    }

    public function down()
    {
        Schema::create('nextlevels_postwidget_formats_materials', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('f_id')->unsigned();
            $table->integer('m_id')->unsigned();
            $table->decimal('gram_per_unit', 10, 4)->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->primary(['f_id', 'm_id']);
        });
    }
}
