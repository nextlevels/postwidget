<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials8 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->decimal('weight_all_gram_per_unit', 10, 2)->nullable(false)->unsigned(false)->default(null)
                ->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->integer('weight_all_gram_per_unit')->nullable(false)->unsigned()->default(null)->change();
        });
    }
}
