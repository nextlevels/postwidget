<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetDiscounts extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_discounts', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('min_items')->nullable()->default(0);
            $table->integer('max_items')->nullable()->unsigned()->default(1000000000);
            $table->double('discount', 10, 0);
            $table->integer('product_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_discounts');
    }
}
