<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetDiscounts3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->integer('material_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->dropColumn('material_id');
        });
    }
}
