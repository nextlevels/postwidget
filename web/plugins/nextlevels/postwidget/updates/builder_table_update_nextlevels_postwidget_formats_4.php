<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormats4 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->integer('product_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->dropColumn('product_id');
        });
    }
}
