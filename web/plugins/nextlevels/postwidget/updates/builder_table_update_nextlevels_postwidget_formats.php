<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormats extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('name')->change();
            $table->string('alignment')->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->string('name', 191)->change();
            $table->string('alignment', 191)->change();
        });
    }
}
