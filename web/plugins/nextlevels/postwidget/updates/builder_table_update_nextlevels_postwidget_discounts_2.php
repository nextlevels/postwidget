<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetDiscounts2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->renameColumn('discount', 'value');
            $table->dropColumn('product_id');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_discounts', function ($table) {
            $table->renameColumn('value', 'discount');
            $table->integer('product_id');
        });
    }
}
