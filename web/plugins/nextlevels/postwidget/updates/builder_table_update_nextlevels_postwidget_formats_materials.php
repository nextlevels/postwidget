<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormatsMaterials extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->decimal('gram_per_unit', 10, 0)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->decimal('gram_per_unit', 10, 0)->nullable(false)->change();
        });
    }
}
