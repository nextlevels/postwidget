<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetShippingMethods extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_shipping_methods', function ($table) {
            $table->decimal('price', 10, 2)->nullable(false)->unsigned(false)->default(0.00)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_shipping_methods', function ($table) {
            $table->double('price', 10, 0)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
