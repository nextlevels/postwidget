<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetPackingPrices extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_postwidget_discounts', 'nextlevels_postwidget_packing_prices');
    }

    public function down()
    {
        Schema::rename('nextlevels_postwidget_packing_prices', 'nextlevels_postwidget_discounts');
    }
}
