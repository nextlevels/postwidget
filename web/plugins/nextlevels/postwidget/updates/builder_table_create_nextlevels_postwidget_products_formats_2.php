<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetProductsFormats2 extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_products_formats', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('f_id')->unsigned();
            $table->double('price', 10, 0)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['p_id', 'f_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_formats');
    }
}
