<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetShippingMethods extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_shipping_methods', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('delivery_time');
            $table->text('description')->nullable();
            $table->double('price', 10, 0);
            $table->integer('max_pieces')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_shipping_methods');
    }
}
