<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetCustomer extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_customer', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('salutation');
            $table->string('forename');
            $table->string('name');
            $table->string('street');
            $table->integer('house_number');
            $table->integer('zip');
            $table->string('place');
            $table->string('country');
            $table->string('email');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_customer');
    }
}
