<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetProductsMaterials extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_products_materials', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('m_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['p_id', 'm_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_materials');
    }
}
