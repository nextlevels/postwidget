<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetProductsFormats2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_products_formats', function ($table) {
            $table->decimal('price', 10, 2)->nullable(false)->unsigned(false)->default(0.00)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_products_formats', function ($table) {
            $table->double('price', 10, 0)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
