<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->decimal('weight', 10, 0)->nullable(false)->unsigned(false)->default(0)->change();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->double('weight', 10, 0)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
