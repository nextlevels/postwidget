<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableDeleteNextlevelsPostwidgetProductsFormats extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_formats');
    }

    public function down()
    {
        Schema::create('nextlevels_postwidget_products_formats', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('f_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->double('price', 10, 0);
            $table->primary(['p_id', 'f_id']);
        });
    }
}
