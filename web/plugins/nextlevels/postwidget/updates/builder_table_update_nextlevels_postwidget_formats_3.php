<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormats3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->integer('alignment_id')->nullable();
            $table->dropColumn('alignment');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats', function ($table) {
            $table->dropColumn('alignment_id');
            $table->string('alignment', 191)->nullable();
        });
    }
}
