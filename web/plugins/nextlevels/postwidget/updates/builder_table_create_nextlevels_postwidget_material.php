<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetMaterial extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_material', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->double('weight', 10, 0);
            $table->string('type')->nullable();
            $table->double('price', 10, 0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_material');
    }
}
