<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetCustomers extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_postwidget_customer', 'nextlevels_postwidget_customers');
    }

    public function down()
    {
        Schema::rename('nextlevels_postwidget_customers', 'nextlevels_postwidget_customer');
    }
}
