<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableCreateNextlevelsPostwidgetProductsSmethods extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_postwidget_products_smethods', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('p_id')->unsigned();
            $table->integer('s_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['p_id', 's_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('nextlevels_postwidget_products_smethods');
    }
}
