<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetFormatsMaterials3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->decimal('price', 10, 2)->nullable();
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_formats_materials', function ($table) {
            $table->dropColumn('price');
        });
    }
}
