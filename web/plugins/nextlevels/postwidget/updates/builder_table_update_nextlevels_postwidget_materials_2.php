<?php namespace Nextlevels\Postwidget\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class BuilderTableUpdateNextlevelsPostwidgetMaterials2 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->dropColumn('price');
        });
    }

    public function down()
    {
        Schema::table('nextlevels_postwidget_materials', function ($table) {
            $table->double('price', 10, 0);
        });
    }
}
