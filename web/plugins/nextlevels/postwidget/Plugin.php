<?php namespace Nextlevels\Postwidget;

use Nextlevels\Postwidget\Components\Products;
use Nextlevels\Postwidget\Components\Widget;
use Nextlevels\Postwidget\Models\Settings;
use System\Classes\PluginBase;

/**
 * Class Plugin
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Plugin extends PluginBase
{

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            Widget::class => 'widget',
            Products::class => 'products'
        ];
    }

    /**
     * @return array
     */
    public function registerSettings(): array
    {
        return [
            'settings' => [
                'label'       => 'Benutzer Einstellungen',
                'description' => 'Einstellen von Texten usw.',
                'category'    => 'Settings',
                'icon'        => 'icon-leaf',
                'class'       => Settings::class,
                'order'       => 69,
                'keywords'    => 'postwidget settings'
            ]
        ];
    }
}
