<?php namespace Nextlevels\Postwidget\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Carbon;
use Nextlevels\Postwidget\Models\Customer;
use Nextlevels\Postwidget\Models\Format;
use Nextlevels\Postwidget\Models\Order;
use Nextlevels\Postwidget\Models\Product;
use Nextlevels\Postwidget\Models\Settings;
use October\Rain\Network\Http;
use October\Rain\Support\Facades\Flash;

/**
 * Class Widget
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Widget extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'widget Component',
            'description' => 'No description provided yet...'
        ];
    }

    /**
     * @return mixed
     */
    public function onZipConfirm()
    {
        $data = \Input::all();
        $rules = [
            'form.zip'      => 'required_without:form.Lat,form.Long',
            'form.zip_name' => 'required_without:form.Lat,form.Long',
            'form.Lat'      => 'required_without:form.zip,form.zip_name',
            'form.Long'     => 'required_without:form.zip,form.zip_name',
            'form.radius'   => 'required',
            'form.radio'    => 'required'

        ];
        $customMessages = [
            'form.zip_name.required_without'
            => 'Bitte wählen sie einen Ort aus der Google Maps Liste aus, oder lassen Sie sich Orten.'
        ];
        $customAttributes = [
            'form.zip'  => 'Postleitzahl',
            'form.Lat'  => 'Localisierung',
            'form.Long' => 'Localisierung'
        ];

        $validator = \Validator::make($data, $rules, $customMessages, $customAttributes);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        if ($data['t-start'] !== 'null') {
            $data['form']['tstart'] = $data['t-start'];
        }

        $this->page['locationInformation'] = $data['form'];
        \Session::put('locationInformation', $data['form']);

        if (! \Session::has('frontend_overview') && ! \Session::has('config_information')) {
            $this->page['products'] = Product::all();

            \Session::put('key', 'value');

            return [
                '#widget' => $this->renderPartial('config')
            ];
        }

        $this->page['data'] = \Session::get('frontend_overview')['overview'];
        $this->page['weight'] = \Session::get('frontend_overview')['weight'];

        return [
            '#widget' => $this->renderPartial('config')
        ];
    }

    /**
     * @return array
     */
    public function onConfigConfirm()
    {
        $data = \Input::all();
        $rules = [
            'config.product'         => 'required',
            'config.format'          => 'required',
            'config.material'        => 'required',
            'config.shipping_method' => 'required',
        ];
        $customAttributes = [
            'config.product'         => 'Produkt',
            'config.format'          => 'Format',
            'config.material'        => 'Material',
            'config.shipping_method' => 'Versand',
        ];

        $validator = \Validator::make($data, $rules, [], $customAttributes);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $productID = $data['config']['product'];
        $formatID = $data['config']['format'];
        $materialID = $data['config']['material'];
        $shippingMethodID = $data['config']['shipping_method'];

        $product = Product::findOrFail($productID);
        $format = $product->formats->where('id', $formatID)->first();
        $material = $format->materials->where('id', $materialID)->first();
        $shipping_method = $product->shippingMethods->where('id', $shippingMethodID)->first();

        if ($product !== null && $format !== null && $material !== null && $shipping_method !== null) {
            $overview =
                $product->name . ', ' . $format->fullInfoList . ' ' . $material->fullInfoList;

            $weight = $format->materials->where('id', $material->id)->first()->weight_all_gram_per_unit;

            \Session::put('frontend_overview', [
                'overview' => $overview,
                'weight'   => $weight
            ]);
            \Session::put('config_information', [
                'product_id'         => $productID,
                'format_id'          => $formatID,
                'material_id'        => $materialID,
                'shipping_method_id' => $shippingMethodID,
            ]);

            return [
                '#productSection' => $this->renderPartial(
                    'productsection/overview',
                    [
                        'data'   => $overview,
                        'weight' => $weight
                    ]
                )
            ];
        }

        Flash::error('Beim Speichern ist ein Fehler aufgetreten.');

        return ['#flash-message' => $this->renderPartial('flash')];
    }

    /**
     * @return array
     */
    public function onProduct()
    {
        $request = \Input::all();
        $product = Product::findOrFail($request['product_id']);
        $this->page['formats'] = $product->formats;
        $this->page['shipping_methods'] = $product->shippingMethods;

        return [
            '#formats'          => $this->renderPartial('config/formats'),
            '#materials'        => $this->renderPartial('config/materials'),
            '#shipping_methods' => $this->renderPartial('config/shipping_methods'),
        ];
    }

    /**
     * @return array
     */
    public function onFormat()
    {
        $request = \Input::all();
        $format = Format::findOrFail($request['format_id']);
        $this->page['materials'] = $format->materials;

        return [
            '#materials' => $this->renderPartial('config/materials')
        ];
    }

    /**
     * @return array
     */
    public function onZipReset()
    {
        \Session::forget('locationInformation');

        return [
            '#widget' => $this->renderPartial('zip-finder')
        ];
    }

    /**
     * @return array
     */
    public function onConfigReset()
    {
        \Session::forget('frontend_overview');
        \Session::forget('config_information');

        return [
            '#productSection' => $this->renderPartial('productsection/config', [
                'products' => Product::all()
            ])
        ];
    }

    /**
     * @return array
     */
    public function onCalculatePrices()
    {
        $data = \Input::all();
        $rules = [
            'places' => 'required'
        ];
        $customAttributes = [
            'places' => 'Orte'
        ];
        $validator = \Validator::make($data, $rules, [], $customAttributes);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $places = $data['places'];
        $prices = $this->calculatePrice($data);

        return [
            '#pricesOverview' => $this->renderPartial('productsection/overview/prices_overview', [
                'productPriceAlone' => $prices['productPriceAlone'],
                'productPrice'      => $prices['productPrice'],
                'shippingPrice'     => $prices['sendingCosts']
            ]),
            '#placesOverview' => $this->renderPartial('productsection/overview/places_overview', [
                'places' => $places
            ])
        ];
    }

    /**
     * @return array
     */
    public function onOrderConfirm()
    {
        $data = \Input::all();
        $rules = [
            'places' => 'required'
        ];
        $customAttributes = [
            'places' => 'Orte'
        ];
        $validator = \Validator::make($data, $rules, [], $customAttributes);
        $settings = Settings::instance();

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $prices = $this->calculatePrice($data);
        $posttaskInfo = isset($settings->posttask_info) ? $settings->posttask_info : null;

        $information = [
            'productPriceAlone'      => $prices['productPriceAlone'],
            'productPrice'           => $prices['productPrice'],
            'shippingPrice'          => $prices['sendingCosts'],
            'sumCostumShippingPrice' => $prices['sumCostumShippingPrice'],
            'allCosts'               => $prices['productPrice'] + $prices['sendingCosts'],
            'places'                 => $data['places'],
            'product'                => \Session::get('frontend_overview')['overview'],
            'locationInformation'    => \Session::get('locationInformation')
        ];

        \Session::put('order_information', $information);

        return [
            '#widget' => $this
                ->renderPartial('order-form', [
                    'data'           => $information,
                    'shippingMethod' => $this->getShippingMethod(),
                    'posttaskInfo'   => $posttaskInfo
                ])
        ];
    }

    /**
     * @param $data
     *
     * @return array
     */
    public function calculatePrice($data)
    {
        $volume = 0;
        $sendingCosts = 0.00;
        $places = $data['places'];
        $configInfo = \Session::get('config_information');
        $product = Product::findOrFail($configInfo['product_id']);
        $format = $product->formats->where('id', $configInfo['format_id'])->first();
        $material = $format->materials->where('id', $configInfo['material_id'])->first();
        $shipping_method = $product->shippingMethods->where('id', $configInfo['shipping_method_id'])->first();
        $customShippingPrice = $shipping_method->price;

        foreach ($places as $place) {
            $volume += $place['volume'];
            $sendingCosts += $place['sendingCosts'];
        }

        if ($product !== null && $format !== null && $material !== null && $shipping_method !== null) {
            $volume = round($volume, -3);

            $productPriceAlone = $product->price * $volume;
            $basePrice = $material->base_price;
            $stackPrice = $material->stack_price * $volume/1000;

            $sumCustomShippingPrice = 0;
            if ($shipping_method->id === 2) {
                $sumCustomShippingPrice = $material->express_stack_price * $volume/1000;
            }

            $productPrice = ($basePrice + $stackPrice + $sumCustomShippingPrice + $productPriceAlone);

            return [
                'productPriceAlone'      => $productPriceAlone,
                'productPrice'           => $productPrice,
                'sendingCosts'           => $sendingCosts,
                'sumCostumShippingPrice' => $sumCustomShippingPrice
            ];
        }
    }

    /**
     * @return array
     */
    public function onSendOrder()
    {
        $data = \Input::all();
        $rules = [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'privacy'    => 'required'
        ];
        $customAttributes = [
            'first_name' => 'Vorname',
            'last_name'  => 'Nachname',
            'email'      => 'E-Mail',
            'privacy'    => 'Datenschutzbestätigung'
        ];
        $validator = \Validator::make($data, $rules, [], $customAttributes);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $configInfo = \Session::get('config_information');
        $orderInfo = \Session::get('order_information');

        $remainingStock = isset($data['is_remaining_stock']);

        $product = Product::findOrFail($configInfo['product_id']);
        $format = $product->formats->where('id', $configInfo['format_id'])->first();
        $material = $format->materials->where('id', $configInfo['material_id'])->first();
        $shipping_method = $product->shippingMethods->where('id', $configInfo['shipping_method_id'])->first();

        if ($product !== null && $format !== null && $material !== null && $shipping_method !== null) {
            $customer = Customer::create($data);

            $order = new Order();
            $order->product = $product;
            $order->material = $material;
            $order->format = $format;
            $order->shippingMethod = $shipping_method;
            $order->customer = $customer;
            $order->ordered_at = Carbon::now();
            $order->zips = $orderInfo['places'];
            $order->save();

            $data['customer'] = $order->customer;
            $data['product'] = $order->product;
            $data['material'] = $order->material;
            $data['format'] = $order->format;
            $data['shippingMethod'] = $order->shippingMethod;
            $data['order'] = $order;
            $data['orderInfo'] = $orderInfo;
            $data['remainingStock'] = $remainingStock;
            $data['locationInformation'] = \Session::get('locationInformation');

            \Mail::send('nextlevels.postwidget::mail.customer-order-confirm', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('Druckerei Jakobs, Produktanfrage');
            });

            \Mail::send('nextlevels.postwidget::mail.order', $data, function ($message) use ($data) {
                $message->to('info@druckerei-jakobs.de');
                $message->subject('Kalkulator-Tool Produktanfrage, Nutzer: '
                    . $data['customer']['id']
                    . ', Order : '
                    . $data['order']['id']);
            });

            \Session::forget('frontend_overview');
            \Session::forget('config_information');
            \Session::forget('order_information');
            \Session::forget('locationInformation');

            return [
                '#widget' => $this->renderPartial('success')
            ];
        }
    }

    public function getShippingMethod()
    {
        $configInfo = \Session::get('config_information');
        $product = Product::findOrFail($configInfo['product_id']);

        return $product->shippingMethods->where('id', $configInfo['shipping_method_id'])->first();
    }

    /**
     * On run
     */
    public function onRun()
    {
        if (\Session::has('locationInformation') && ! \Session::has('frontend_overview')) {
            $this->page['locationInformation'] = \Session::get('locationInformation');
            $this->page['products'] = Product::all();
        }
        if (\Session::has('locationInformation')
            && \Session::has('frontend_overview')
            && \Session::has('config_information')) {
            $this->page['locationInformation'] = \Session::get('locationInformation');
            $this->page['data'] = \Session::get('frontend_overview')['overview'];
            $this->page['weight'] = \Session::get('frontend_overview')['weight'];
        }
    }

    public function onCallPost()
    {
        $data = \Input::all();
        $save = false;
        $dir = 'storage/tmpdata/dpfiles/';

        if (isset($data['save'])) {
            array_forget($data, 'save');
            $save = true;
        }

        $response = Http::post(
            'https://postaktuell-manager.deutschepost.de/Postaktuell/Postaktuell',
            function ($http) use ($data) {
                $http->data($data);
            }
        );

        if ($save) {
            $responseExploded = explode('+', $response);
            $urlstring = trim($responseExploded[8], " \n");
            $url = explode('/', $urlstring);
            $newPath = $dir . end($url);
            $data = file_get_contents($urlstring);
            $handle = fopen($newPath, 'w');
            fwrite($handle, $data);
            fclose($handle);
            $responseExploded[8] = $newPath;
            $response = implode('+', $responseExploded);

            $directory = array_diff(scandir($dir), ['..', '.', '.gitignore']);
            if (count($directory) > 30) {
                array_splice($directory, -5);
                foreach ($directory as $value) {
                    unlink($dir . $value);
                }
            }
        }

        return $response;
    }
}
