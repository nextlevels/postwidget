<?php namespace Nextlevels\Postwidget\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\ReorderController;
use Backend\Classes\Controller;
use BackendMenu;

/**
 * Class ShippingMethods
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class ShippingMethods extends Controller
{

    /**
     * @var array
     */
    public $implement = [ListController::class, FormController::class, ReorderController::class];

    /**
     * @var string config files
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    /**
     * ShippingMethods constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nextlevels.Postwidget', 'widget', 'widget.shipping-methods');
    }
}
