<?php namespace Nextlevels\Postwidget\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ImportExportController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use BackendMenu;

/**
 * Class Customers
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Customers extends Controller
{

    /**
     * @var array
     */
    public $implement = [
        ListController::class,
        FormController::class,
        ImportExportController::class
    ];

    /**
     * @var string config files
     */
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    /**
     * Customers constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nextlevels.Postwidget', 'widget', 'widget.customers');
    }
}
