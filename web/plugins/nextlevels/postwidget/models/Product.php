<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Class Product
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_products';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array
     */
    public $belongsToMany = [
        'shippingMethods' => [
            ShippingMethod::class,
            'table'    => 'nextlevels_postwidget_products_smethods',
            'key'      => 'p_id',
            'otherKey' => 's_id'
        ]
    ];

    public $hasMany = [
        'formats'         => [
            Format::class
        ],
        'materials'         => [
            Material::class
        ]
    ];

    /**public $hasMany = [
        'discounts' => Discount::class
    ];**/
}
