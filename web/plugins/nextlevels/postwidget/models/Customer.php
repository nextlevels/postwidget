<?php namespace Nextlevels\Postwidget\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Class Customer
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Customer extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_customers';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array
     */
    public $fillable = [
        'salutation',
        'first_name',
        'last_name',
        'email'
    ];

    /**
     * @var array
     */
    public $hasMany = [
        'orders' => Order::class
    ];

    protected $appends = ['fullInfoList'];

    /**
     * @return mixed
     */
    public function getFullInfoListAttribute()
    {
        return $this->
            first_name . ' ' . $this->last_name . ' ' . $this->email;
    }
}
