<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Class Format
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Format extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_formats';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array
     */
    public $belongsTo = [
        'alignment' => Alignment::class,
        'product' => Product::class
    ];

    public $hasMany = [
        'materials' => Material::class
    ];

    /**
     * @var array
     */
    protected $appends = ['fullInfoList'];

    /**
     * @return mixed
     */
    public function getFullInfoListAttribute()
    {
        return $this->
            name
            . ' ('
            . $this->height
            . 'x' . $this->width
            . 'cm, '
            . $this->pages
            . ' Seiten, '
            . $this->alignment['name']
            . ') ';
    }
}
