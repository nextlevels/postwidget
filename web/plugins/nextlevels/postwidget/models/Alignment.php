<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Class Alignment
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Alignment extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_alignments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
