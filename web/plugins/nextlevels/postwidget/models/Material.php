<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Class Material
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Material extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_materials';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array
     */
    protected $appends = ['fullInfoList'];

    public $hasMany = [
        'packing_prices'    => [
            PackingPrice::class
        ],
        'print_prices'         => [
            PrintPrice::class
        ],
    ];

    public $belongsTo = [
        'format' => Format::class
    ];

    /**
     * @return mixed
     */
    public function getFullInfoListAttribute()
    {
        return $this->weight . ' g  ' . $this->type;
    }
}
