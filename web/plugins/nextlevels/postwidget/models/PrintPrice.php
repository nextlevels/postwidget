<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Model
 */
class PrintPrice extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_print_prices';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
