<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Class Order
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_orders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var
     */
    protected $jsonable = ['zips'];

    /**
     * @var array
     */
    public $belongsTo = [
        'customer' => Customer::class,
        'product' => Product::class,
        'material' => Material::class,
        'format' => Format::class,
        'shippingMethod' => ShippingMethod::class
    ];
}
