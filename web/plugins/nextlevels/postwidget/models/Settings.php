<?php namespace Nextlevels\Postwidget\Models;

use System\Behaviors\SettingsModel;

/**
 * Class Settings
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class Settings extends \Model
{
    public $implement = [SettingsModel::class];
    public $settingsCode = 'nextlevels_postwidget_settings';
    public $settingsFields = 'fields.yaml';
}
