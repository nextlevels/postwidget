<?php namespace Nextlevels\Postwidget\Models;

use Model;

/**
 * Model
 */
class PackingPrice extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_packing_prices';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
