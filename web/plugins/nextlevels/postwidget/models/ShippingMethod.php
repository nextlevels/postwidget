<?php namespace Nextlevels\Postwidget\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Class ShippingMethod
 *
 * @author Jan Kirsten <jan.kirsten@next-levels.de>, Next Levels GmbH
 */
class ShippingMethod extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_postwidget_shipping_methods';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
