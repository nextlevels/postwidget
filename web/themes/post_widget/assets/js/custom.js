var errorBox = $(".error-text");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(handlePosition, showError);
    } else {
        errorBox.innerHTML = "Geolocation wird von Ihrem Browser nicht unterstützt.";
    }
}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            errorBox.innerHTML = "Der Benutzer hat die Localisierung abgelehnt, geben Sie Ihre PLZ bitte in die Standortsuche ein.";
            break;
        case error.POSITION_UNAVAILABLE:
            errorBox.innerHTML = "Localisierungs Informationen sind nicht vorhanden, geben Sie Ihre PLZ bitte in die Standortsuche ein.";
            break;
        case error.TIMEOUT:
            errorBox.innerHTML = "Zeitüberschreitung der Anforderung, geben Sie Ihre PLZ bitte in die Standortsuche ein."
            break;
        case error.UNKNOWN_ERROR:
            errorBox.innerHTML = "Ein unbekannter Fehler ist aufgetreten, geben Sie Ihre PLZ bitte in die Standortsuche ein.";
            break;
    }
}

function handlePosition(position) {
    $("#autocomplete").prop('disabled', true);
    $("#autocomplete").attr("placeholder", "Sie wurden gefunden. Lat: " + position.coords.latitude + ", Long: " + position.coords.longitude);
    $("#formLat").val(position.coords.latitude);
    $("#formLong").val(position.coords.longitude);
}