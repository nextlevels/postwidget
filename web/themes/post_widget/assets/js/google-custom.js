var map;
var places = [];
var Layer = [];
var currentBounds;
var currentZipsCount;
var currentLocation;
var excludedZBNArrGlobal = [];
var myParser = null;
var radiusCircle = null;
var mappedZipArr = [];
var mappedShownZipArr = [];

function initAutocomplete(input) {
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setComponentRestrictions({'country': ['de']});

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        $('#formZipName').val(autocomplete.getPlace().name);
    });
}

function initTdate() {
    var today = new Date().addDays(12);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var startDate = yyyy + '-' + mm + '-' + dd;

    $('.t-datepicker').tDatePicker({
        startDate: startDate,
        titleCheckIn: 'Postaufgabe',
        titleDays: ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'],
        titleMonths: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'Septemper', 'Oktober', 'November', "Dezember"],
        titleToday: 'Heute',
        dateRangesShowTitle: false
    });
}

function initMap(zipInfo, setArea, lat, long) {
    if (zipInfo !== '') {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': zipInfo
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: results[0].geometry.location,
                    zoom: 12
                });
                setMapOptions(setArea);
            } else {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 52.520008, lng: 13.404954},
                    zoom: 8
                });
                setMapOptions(setArea);
            }
        });
    } else {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: long},
            zoom: 12
        });
        setMapOptions(setArea);
    }
}

function setMapOptions(setArea) {
    if (setArea === 1) {
        google.maps.event.addListener(map, 'click', function (event) {
            $(".loading-indicator").show();
            places = [];
            excludedZBNArrGlobal = [];
            if (typeof geoXmlDoc !== 'undefined') {
                for (var f = 0; f < geoXmlDoc.markers.length; f++) {
                    geoXmlDoc.markers[f].setMap(null);
                }
            }
            var clickedLocation = event.latLng;
            callResolveAddress(clickedLocation, excludedZBNArrGlobal);
        });
    } else {
        getZipCodeData();

        google.maps.event.addListener(map, 'click', function (event) {
            var clickedLocation = event.latLng;
            $(".loading-indicator").show();

            $.request('onCallPost', {
                data: {
                    Lat: clickedLocation.lat(),
                    Lon: clickedLocation.lng(),
                    ID: 'EQN770YJHP43C7GT1PSS',
                    UID: 'K81AI',
                    reset: 1,
                    action: 'PLZbyCoords'
                },
                success: function (result) {
                    var zip = result.replace("\n", "");
                    mappedShownZipArr.push(zip);
                    resolveZipPost($('#weight').val(), $('#producttype').val(), parseInt(zip));
                }
            });
        });
        google.maps.event.addListener(map, 'center_changed', function () {
            getZipCodeData();
        });
    }

    $(".loading-indicator").hide();
    handleMapDisable();
}

function resolveZipPost(weight, producttype, plz) {
    $.request('onCallPost', {
        data: {
            'PLZ': plz,
            'Produktvariante': producttype,
            'Gewicht': weight,
            'DoSave': 0,
            'CreateKML': 1,
            'ID': 'EQN770YJHP43C7GT1PSS',
            'UID': 'K81AI',
            'reset': 1,
            'action': 'ResolvePLZ'
        },
        success: function (result) {
            var resultArr = result.split('+');
            var volume = parseFloat(resultArr[2]) + parseFloat(resultArr[3]) + parseFloat(resultArr[4]);
            var sendingCosts = parseFloat(resultArr[5].replace('.', '').replace(',', '.')) + parseFloat(resultArr[6].replace('.', '').replace(',', '.')) + parseFloat(resultArr[7].replace('.', '').replace(',', '.'));
            var kmlLayerLink = resultArr[8];

            places.push({zip: plz, volume: volume, sendingCosts: sendingCosts, kmlLink: kmlLayerLink});

            Layer[plz] = new google.maps.KmlLayer({
                url: kmlLayerLink,
                suppressInfoWindows: true,
                map: map,
                preserveViewport: true
            });
            Layer[plz].addListener('click', function (event) {
                $(".loading-indicator").show();
                this.setMap(null);
                for (var i = 0; i < places.length; i++) {
                    if (places[i].kmlLink === this.url) {
                        places.splice(i, 1);
                    }
                }
                if (places.length > 0) {
                    $.request('onCalculatePrices', {
                        data: {'places': places}
                    }).done(function () {
                        $(".loading-indicator").hide();
                    })
                } else {
                    $('#pricesOverviewContainer').remove();
                    $('#placesOverviewContainer').remove();
                    $(".loading-indicator").hide();
                }

            });

            $.request('onCalculatePrices', {
                data: {'places': places}
            }).done(function () {
                $(".loading-indicator").hide();
            });
        }
    });
}

function deletePlace(zip) {
    $(".loading-indicator").show();

    if ($("#setArea").val() === '1') {
        excludedZBNArrGlobal.push(zip);

        for (var f = 0; f < geoXmlDoc.markers.length; f++) {
            geoXmlDoc.markers[f].setMap(null);
        }
        if (places.length > 0) {
            places = [];

            callResolveAddress(currentLocation, excludedZBNArrGlobal);

            var deleteInterval = setInterval(function () {
                if (places.length === currentZipsCount) {
                    if (places.length === 0) {
                        $('#pricesOverviewContainer').remove();
                        $('#placesOverviewContainer').remove();
                        geoXmlDoc.placemarks = [];
                        clearInterval(deleteInterval);
                        $(".loading-indicator").hide();
                    } else {
                        $.request('onCalculatePrices', {
                            data: {'places': places}
                        });
                        geoXmlDoc.placemarks = [];
                        clearInterval(deleteInterval);
                        $(".loading-indicator").hide();
                    }
                }
            }, 1000);
        } else {
            $('#pricesOverviewContainer').remove();
            $('#placesOverviewContainer').remove();
        }
    } else {
        zip = parseFloat(zip);
        for (var i = 0; i < places.length; i++) {
            if (places[i].zip === zip) {
                places.splice(i, 1);
            }
        }
        mappedShownZipArr.splice(mappedShownZipArr.indexOf(zip),1);
        Layer[zip].setMap(null);
        if (places.length > 0) {
            $.request('onCalculatePrices', {
                data: {'places': places}
            }).done(function () {
                $(".loading-indicator").hide();
            });
        } else {
            $('#pricesOverviewContainer').remove();
            $('#placesOverviewContainer').remove();
            $(".loading-indicator").hide();
        }
    }
}

function removeMapData() {
    if (Layer.length > 0) {
        for (var i = 0; i < places.length; i++) {
            Layer[places[i].zip].setMap(null);
        }
    } else if (typeof geoXmlDoc !== 'undefined') {
        for (var f = 0; f < geoXmlDoc.markers.length; f++) {
            geoXmlDoc.markers[f].setMap(null);
        }
    }
    if (radiusCircle !== null) {
        radiusCircle.setMap(null);
    }
    places = [];
    $('#pricesOverviewContainer').remove();
    $('#placesOverviewContainer').remove();
    handleMapDisable(false);
}

function callResolveAddress(clickedLoc, excludedZBNArr) {
    var radius = $('#radius').val();
    var prodtype = $('#producttype').val();
    var excludedZBNStr = (excludedZBNArr.length > 0) ? excludedZBNArr.join(';') : '';
    currentLocation = clickedLoc;

    if (radiusCircle !== null) {
        radiusCircle.setMap(null);
    }

    radiusCircle = new google.maps.Circle({
        strokeColor: '#00325e',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#00325e',
        fillOpacity: 0.35,
        map: map,
        center: {lat: currentLocation.lat(), lng: currentLocation.lng()},
        radius: radius * 1000
    });

    $.request('onCallPost', {
        data: {
            save: true,
            Lat: clickedLoc.lat(),
            Lon: clickedLoc.lng(),
            ID: 'EQN770YJHP43C7GT1PSS',
            UID: 'K81AI',
            reset: 1,
            action: 'ResolveAddress',
            Produktvariante: prodtype,
            Gewicht: 5,
            Radius: radius,
            Zbnmode: 1,
            ZBN: excludedZBNStr,
            SelMode: 0
        },
        success: function (result) {
            var resultArr = result.result.split('+');

            var myParser = new geoXML3.parser({map: map, zoom: false, afterParse: getPlaceFromKml});
            myParser.parse(resultArr[8]);
        }
    });
}

function getPlaceFromKml(doc) {
    currentBounds = map.getBounds();

    if (!currentBounds) currentBounds = new google.maps.LatLngBounds();

    geoXmlDoc = doc[0];
    iterateThroughKml(geoXmlDoc, doc);
}

function iterateThroughKml(geoXmlDoc, doc) {
    var weight = $('#weight').val();
    var prodType = $('#producttype').val();
    currentZipsCount = (geoXmlDoc.placemarks.length);
    if (currentZipsCount > 0) {
        for (var i = 0; i < currentZipsCount; i++) {
            var currentZip = doc[0].placemarks[i].name;
            getZipInfo(currentZip, prodType, weight)
        }
    } else {
        $("#placesOverview").empty().append('<div id="placesOverviewContainer" class="product-config"><div class="error-text">Keine Versandorte in diesem Gebiet verfügbar.</div></div>');
        $(".loading-indicator").hide();
    }
}

function getZipInfo(currentZip, prodType, weight) {
    var myInterval = setInterval(function () {
        if (places.length === currentZipsCount) {
            $.request('onCalculatePrices', {
                data: {'places': places}
            });
            geoXmlDoc.placemarks = [];
            clearInterval(myInterval);
            $(".loading-indicator").hide();
        }
    }, 1000);

    $.request('onCallPost', {
        data: {
            'ZBN': currentZip,
            'Produktvariante': prodType,
            'Gewicht': weight,
            'DoSave': 0,
            'CreateKML': 1,
            'ID': 'EQN770YJHP43C7GT1PSS',
            'UID': 'K81AI',
            'reset': 1,
            'action': 'RecalcZBN'
        },
        success: function (result) {
            var resultArr = result.split('+');
            var volume = parseFloat(resultArr[2]) + parseFloat(resultArr[3]) + parseFloat(resultArr[4]);
            var sendingCosts = parseFloat(resultArr[5].replace('.', '').replace(',', '.')) + parseFloat(resultArr[6].replace('.', '').replace(',', '.')) + parseFloat(resultArr[7].replace('.', '').replace(',', '.'));

            places.push({zip: currentZip, volume: volume, sendingCosts: sendingCosts, kmlLink: resultArr[8]});
        }
    });
}

function handleMapDisable(indicator) {
    if (($("#isProductSet").val() === 'set' && indicator !== false) || indicator) {
        $(".mapdisable").hide();
        $("#dottedMap").fadeIn();
        $("#dottedProductConfig").fadeOut();
    } else {
        $(".mapdisable").css('display', 'flex');
        $("#dottedMap").fadeOut();
        var dottedProduct = setInterval(function () {
            if ($("#dottedProductConfig").length) {
                $("#dottedProductConfig").fadeIn();
                clearInterval(dottedProduct);
            }
        })

    }
}

function getMapCenter() {
    var lat = map.getCenter().lat();
    var lng = map.getCenter().lng();
    return {'lat': lat, 'lng': lng};
}

function getZipCodeData() {
    var mapCenter = getMapCenter();

    $.ajax({
        type: "GET",
        url: "https://public.opendatasoft.com/api/records/1.0/search/",
        data: {
            'dataset': 'postleitzahlen-deutschland',
            'rows': 50,
            'facet': 'note',
            'facet': 'plz',
            'geofilter.distance': mapCenter.lat + ',' + mapCenter.lng + ',' + map.getZoom() * 500
        },
        success: function (result) {
            var records = result.records;
            for (var i = 0; i < records.length; i++) {
                var plz = records[i].fields.plz;

                if ($.inArray(plz, mappedZipArr) === -1) {
                    mappedZipArr.push(plz);
                    var marker = new google.maps.Marker({
                        position: {lat: records[i].geometry.coordinates[1], lng: records[i].geometry.coordinates[0]},
                        map: map,
                        title: plz
                    });
                    marker.addListener('click', function (event) {
                        $(".loading-indicator").show();
                        var clickedLocation = event.latLng;

                        $.request('onCallPost', {
                            data: {
                                Lat: clickedLocation.lat(),
                                Lon: clickedLocation.lng(),
                                ID: 'EQN770YJHP43C7GT1PSS',
                                UID: 'K81AI',
                                reset: 1,
                                action: 'PLZbyCoords'
                            },
                            success: function (result) {
                                var zip = result.replace("\n", "");

                                if ($.inArray(zip, mappedShownZipArr) === -1) {
                                    mappedShownZipArr.push(zip);
                                    resolveZipPost($('#weight').val(), $('#producttype').val(), parseInt(zip));
                                } else {
                                    deletePlace(zip);
                                }
                            }
                        });
                    });
                }
            }
        }
    })
}