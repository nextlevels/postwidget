module.exports = function (grunt) {

    /**
     * Project configuration.
     */
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        paths: {
            root: '../assets/',
            bower: 'bower_components/',
            node: 'node_modules/',
            scss: '<%= paths.root %>scss/',
            css: '<%= paths.root %>css/',
            fonts: '<%= paths.root %>fonts/',
            img: '<%= paths.root %>images/',
            js: '<%= paths.root %>js/'
        },
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: [
                            'Last 2 versions',
                            'Firefox ESR',
                            'IE 9'
                        ]
                    })
                ]
            },
            dist: {
                src: '<%= paths.css %>layout.css'
            }
        },
        sass: {
            options: {
                outputStyle: 'expanded',
                precision: 8,
                sourceMap: false
            },
            dist: {
                files: {
                    '<%= paths.css %>layout.css': '<%= paths.scss %>theme.scss'
                }
            }
        },
        imagemin: {
            images: {
                files: [
                    {
                        cwd: '<%= paths.img %>',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: '<%= paths.img %>',
                        expand: true
                    }
                ]
            },
            extensionicon: {
                files: {
                    '<%= paths.root %>ext_icon.png': '<%= paths.root %>ext_icon.png'
                }
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: '*',
                advanced: false
            },
            layout: {
                src: '<%= paths.css %>layout.css',
                dest: '<%= paths.css %>layout.min.css'
            }
        },
        uglify: {
            options: {
                compress: {
                    warnings: false
                },
                output: {
                    comments: false
                }
            },
            script: {
                files: {
                    '<%= paths.js %>script.min.js': [
                        '<%= paths.node %>jquery/dist/jquery.js',
                        '<%= paths.node %>uikit/dist/js/uikit.js',
                        '<%= paths.node %>uikit/dist/js/uikit-icons.js',
                        '<%= paths.node %>evoweb-geoxml3/kmz/geoxml3.js',
                        '<%= paths.node %>t-datepicker/public/theme/js/t-datepicker.min.js',
                        '<%= paths.js %>custom.js',
                        '<%= paths.js %>google-custom.js'
                    ]
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: [
                    '<%= paths.scss %>**/*.scss'
                ],
                tasks: ['css']
            },
            javascript: {
                files: [
                    '<%= paths.js %>/custom.js',
                    '<%= paths.js %>/google-custom.js'
                ],
                tasks: ['js']
            }
        }
    });

    /**
     * Register tasks
     */
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    /**
     * Grunt update task
     */
    grunt.registerTask('css', ['sass', 'postcss', 'cssmin']);
    grunt.registerTask('js', ['uglify']);
    grunt.registerTask('image', ['imagemin']);
    grunt.registerTask('build', ['js', 'css', 'image']);
    grunt.registerTask('default', ['build']);
};
